export class Loc {
  name: string;
  type: string;
  average: number;
  point: [number, number];

  constructor(obj) {
    this.name = obj.name;
    this.type = obj.type;
    this.average = obj.average;
    this.point = obj.point;
  }

  setPoint(point): void {
    this.point = point;
  }
}
