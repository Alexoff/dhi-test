import {Component, Input, OnInit} from '@angular/core';
import Map from 'ol/Map';
import View from 'ol/View';
import VectorLayer from 'ol/layer/Vector';
import {defaults as defaultControls} from 'ol/control';
import VectorSource from 'ol/source/Vector';
import * as olProj from 'ol/proj';
import Point from 'ol/geom/Point';
import Feature from 'ol/Feature';
import Overlay from 'ol/Overlay';
import OSM from 'ol/source/OSM';
import TileLayer from 'ol/layer/Tile';
import {Loc} from '../../models/location.model';


@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  @Input() location?: Loc;
  public locations: Loc[] = [];
  public map: Map;
  public layer: VectorLayer;
  public overlay: Overlay;

  constructor() {
  }

  ngOnInit(): void {
    this.initMap();
    this.checkClick();
  }

  initMap(): void {
    this.map = new Map({
      target: 'map',
      layers: [
        new TileLayer({
          source: new OSM()
        })
      ],
      controls: defaultControls({
        rotate: false,
        zoom: false,
        attributionOptions: {
          className: 'custom-attribution'
        }
      }),
      view: new View({
        center: olProj.fromLonLat([7.0785, 51.4614]),
        zoom: 5
      })
    });
  }

  checkClick(): void {
    const content = document.getElementById('popup-content');

    this.map.on('singleclick', (event) => {
      if (this.map.hasFeatureAtPixel(event.pixel) === true && this.overlay) {
        const coordinate = event.coordinate;

        content.innerHTML = `${this.getLocationInfo(coordinate)}`;
        this.overlay.setPosition(coordinate);
      } else if (this.overlay) {
        this.overlay.setPosition(undefined);
      }
    });
  }

  getLocationInfo(coordinate): string {
    const point = olProj.toLonLat(coordinate);
    let data: Loc;
    this.locations.forEach(location => {
      if (location.point[0] - point[0] < 2 && location.point[1] - point[1] < 2) {
        data = location;
      }
    });
    return `<p>Nazwa: ${data.name}</p><p>Typ: ${data.type}</p> <p>Srednia wartość: ${data.average}</p>`;
  }

  addPoint(event): void {
    if (this.location) {
      const coordinate = this.map.getEventCoordinate(event);
      const point = olProj.toLonLat(coordinate);
      this.drawPoint(point[0], point[1]);
      this.addOverlay(point[0], point[1], this.location);
      this.location.setPoint(point);
      this.locations.push(this.location);
      this.location = null;

    }
  }

  drawPoint(lon: number, lat: number): void {
    this.layer = new VectorLayer({
      source: new VectorSource({
        features: [
          new Feature({
            geometry: new Point(olProj.fromLonLat([lon, lat]))
          })
        ]
      })
    });
    this.map.addLayer(this.layer);
  }

  addOverlay(lon, lat, location: Loc): void {
    const container = document.getElementById('popup');
    this.overlay = new Overlay({
      element: container,
      autoPan: true,
      autoPanAnimation: {
        duration: 250
      }
    });
    this.map.addOverlay(this.overlay);
  }

}
