import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Loc} from '../../models/location.model';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  @Output() location: EventEmitter<Loc> = new EventEmitter();
  public locationForm: FormGroup;
  public isAdd = false;
  public data = ['Przepływ nieznany', 'Przeplyw - SUW/ZUW', 'Pzepływ - Zbiernik', 'Przepływ międzystredowy']

  constructor() {
  }

  ngOnInit(): void {
    this.prepareForm();
  }

  prepareForm(): void {
    this.locationForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      type: new FormControl('', [Validators.required]),
      average: new FormControl('', [Validators.required]),
      point: new FormControl('', [Validators.required])
    });
  }

  onSubmit(): void {
    if (this.locationForm.controls.name.invalid || this.locationForm.controls.type.invalid || this.locationForm.controls.average.invalid) {
      this.locationForm.markAllAsTouched();
      return;
    }
    this.location.emit(new Loc(this.locationForm.value));
  }

  addPoint(): void {
  }

  clear(): void {
    this.locationForm.controls.name.setValue('');
    this.locationForm.controls.type.setValue('');
    this.locationForm.controls.average.setValue('');
  }

}
