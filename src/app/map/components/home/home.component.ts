import { Component, OnInit } from '@angular/core';
import {Loc} from '../../models/location.model';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public location: Loc;

  constructor() { }

  ngOnInit(): void {
  }

  onLocation(event): void {
    this.location = event;
  }
}
