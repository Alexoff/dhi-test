import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MapComponent } from './components/map/map.component';
import {MapRoutingModule} from './map-routing.module';
import { HomeComponent } from './components/home/home.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MaterialModule} from '../mateiral/material.module';
import {MatIconModule} from '@angular/material/icon';



@NgModule({
  declarations: [MapComponent, HomeComponent, ToolbarComponent],
  imports: [
    CommonModule,
    MapRoutingModule,
    ReactiveFormsModule,
    MaterialModule,
    MatIconModule
  ]
})
export class MapModule { }
